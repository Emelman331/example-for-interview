package repositories

import (
	"fmt"
	"strings"
	"time"

	asc "github.com/aerospike/aerospike-client-go"
	"github.com/aerospike/aerospike-client-go/types"
	ac "gitlab.regium.com/xr/aerospike/config"
	"gitlab.regium.com/xr/plan-processor/models"
	"gitlab.regium.com/xr/xutor/aerospikedb"
	g "gitlab.regium.com/xr/xutor/global"
)

type AerospikeRepository struct {
	cli           *asc.Client
	workersNumber uint
}

// NewAerospikeRepository returns new instance of AerospikeRepository.
func NewAerospikeRepository(cfg ac.AerospikeConfig, workersCount uint) (*AerospikeRepository, error) {
	// Create client policy.
	cp := asc.NewClientPolicy()
	cp.Timeout = time.Duration(cfg.TimeoutSec) * time.Second

	// Create aerospike hosts list.
	hosts := strings.Split(cfg.Hosts, ",")
	aerospikeHosts := make([]*asc.Host, 0, len(hosts))

	for _, host := range hosts {
		aerospikeHosts = append(aerospikeHosts, asc.NewHost(host, cfg.Port))
	}

	// Create client.
	cli, err := asc.NewClientWithPolicyAndHost(cp, aerospikeHosts...)
	if err != nil {
		return nil, fmt.Errorf("asc.NewClientWithPolicyAndHost() error: %v", err)
	}

	// Change additional params.
	cli.DefaultQueryPolicy.SocketTimeout = time.Duration(cfg.TimeoutSec) * time.Second

	return &AerospikeRepository{
		cli:           cli,
		workersNumber: workersCount}, nil
}

func (o *AerospikeRepository) GetProfile(pid int64) (*asc.Record, error) {
	key, err := asc.NewKey(g.ASPNameSpace, g.ASPProfilesSet, pid)
	if err != nil {
		return nil, fmt.Errorf("cant create key, error: %v", err)
	}

	record, err := o.cli.Get(asc.NewPolicy(), key, g.EmailKeyNamePtrn)
	if err != nil {
		return nil, fmt.Errorf("cant read record, error: %v", err)
	}

	return record, nil
}

// GetCalendar create new calendar or took existed.
func (o *AerospikeRepository) GetCalendar(pid, targetTime int64) (*models.Calendar, error) {
	calendar, err := get(o.cli, pid, targetTime)
	if err != nil {
		errMsg := fmt.Errorf("get(%d, %d) error: %v", pid, targetTime, err)
		return nil, errMsg
	}

	if calendar == nil {
		errMsg := fmt.Errorf("get(%d, %d) return nil", pid, targetTime)
		return nil, errMsg
	}

	return calendar, nil
}

// Return Calendar structure from DB or new instance of Calendar (with dayStart set to today), if there is no such record.
func get(client *asc.Client, pid, startOfDay int64) (*models.Calendar, error) {
	sk := fmt.Sprintf("%d_%d", pid, startOfDay)

	key, err := asc.NewKey(g.ASPNameSpace, g.ASPCalendarsSet, sk)
	if err != nil {
		return nil, err
	}

	rec, err := client.Get(nil, key)
	if err != nil && !aerospikedb.IsAerospikeErrorByCode(err, types.KEY_NOT_FOUND_ERROR) {
		return nil, fmt.Errorf("client.Get(%s.%s.%v) err: %v", g.ASPNameSpace, g.ASPCalendarsSet, sk, err)
	}

	if rec == nil { // calendar not exist - create new and return
		return &models.Calendar{
			Pid:      pid,
			DayStart: startOfDay,
			Slots:    make(map[int64]int),
		}, nil
	}

	if rec.Bins == nil {
		return nil, fmt.Errorf("client.Get(%s.%s.%v) return bins as nil", g.ASPNameSpace, g.ASPCalendarsSet, sk)
	}

	// try create calendar by db data
	calendar, err := tryNewCalendar(rec.Bins)
	if err != nil {
		return nil, fmt.Errorf("tryNewCalendar(%v) err: %v", rec.Bins, err)
	}

	if calendar == nil {
		return nil, fmt.Errorf("tryNewCalendar(%v) return nil", rec.Bins)
	}

	return calendar, nil
}

// Calendar constructor.
func tryNewCalendar(bins asc.BinMap) (*models.Calendar, error) {
	calendar := models.Calendar{Slots: make(map[int64]int)}
	if bins == nil {
		return &calendar, fmt.Errorf("argument exception bins is nil")
	}

	if len(bins) == 0 {
		return &calendar, fmt.Errorf("argument exception bins is empty")
	}

	switch profileID := bins[g.PidKeyNamePtrn].(type) {
	case int64:
		calendar.Pid = profileID
	case int:
		calendar.Pid = int64(profileID)
	default:
		return nil, fmt.Errorf("'%s' unexpected type %T", g.PidKeyNamePtrn, profileID)
	}

	switch dayStart := bins[g.DayStartKeyNamePtrn].(type) {
	case int64:
		calendar.DayStart = dayStart
	case int:
		calendar.DayStart = int64(dayStart)
	default:
		return nil, fmt.Errorf("'%s' unexpected type %T", g.DayStartKeyNamePtrn, dayStart)
	}

	planTimeByEmailID, ok := bins[g.SlotsKeyNamePtrn].(map[interface{}]interface{})
	if !ok {
		return nil, fmt.Errorf("'%s' unexpected type", g.SlotsKeyNamePtrn)
	}

	for k, v := range planTimeByEmailID {
		var ts int64

		var eid int

		switch planTS := k.(type) {
		case int64:
			ts = planTS
		case int:
			ts = int64(planTS)
		default:
			return nil, fmt.Errorf("'%s' unexpected key type %T", g.SlotsKeyNamePtrn, planTS)
		}

		switch emailID := v.(type) {
		case int:
			eid = emailID
		default:
			return nil, fmt.Errorf("'%s' unexpected value type %T", g.SlotsKeyNamePtrn, emailID)
		}

		calendar.Slots[ts] = eid
	}

	return &calendar, nil
}

// Upsert update record in DB or insert if there is not such record.
func (o *AerospikeRepository) Upsert(calendar *models.Calendar, ttlSeconds uint32) error {
	if calendar == nil {
		return fmt.Errorf("calendar is nil")
	}

	key, err := asc.NewKey(
		g.ASPNameSpace,
		g.ASPCalendarsSet,
		fmt.Sprintf("%d_%d", calendar.Pid, calendar.DayStart))
	if err != nil {
		return err
	}

	var writePolicy *asc.WritePolicy
	if ttlSeconds != 0 {
		writePolicy = asc.NewWritePolicy(0, ttlSeconds)
	}

	return o.cli.PutBins(writePolicy, key,
		asc.NewBin("pid", calendar.Pid),
		asc.NewBin("day_start", calendar.DayStart),
		asc.NewBin("slots", calendar.DayStart))
}

// IsAerospikeErrorByCode check error by error code.
func IsAerospikeErrorByCode(err error, code types.ResultCode) bool {
	aspErr, ok := err.(types.AerospikeError)

	return ok && aspErr.ResultCode() == code
}

// Close closes all connections to databases.
func (o *AerospikeRepository) Close() { o.cli.Close() }
