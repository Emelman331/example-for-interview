package repositories

import (
	"fmt"

	"github.com/jinzhu/gorm"
	"gitlab.regium.com/xr/plan-processor/models"
	"gitlab.regium.com/xr/psql"
	"gitlab.regium.com/xr/psql/config"
	"gitlab.regium.com/xr/xutor/data/smartplanner"
	"gitlab.regium.com/xr/xutor/model/sites"
)

// EntitiesRepository is the PostgreSQL entities repository.
type EntitiesRepository struct {
	db *gorm.DB
}

// NewEntitiesRepository creates new EntitiesRepository instance.
func NewEntitiesRepository(cfg config.PostgreSQLConfig) (*EntitiesRepository, error) {
	db, err := psql.GetEntityDatabase(cfg)
	if err != nil {
		return nil, fmt.Errorf("psql.GetEntityDatabase(%v) error: %v", cfg, err)
	}

	return &EntitiesRepository{db: db}, nil
}

// Close closes connections to database.
func (o *EntitiesRepository) Close() error {
	return o.db.Close()
}

// GetPlanProcessor gets plan-processor model.
func (o *EntitiesRepository) GetPlanProcessor() (*models.Processor, error) {
	res := &models.Processor{}
	err := o.db.Table(psql.SPProcessorsTable).First(res, smartplanner.PlanProcessorID).Error

	return res, err
}

// GetRoadmapIDToRoadmapConditions get from entities roadmap conditions by roadmapID.
func (o *EntitiesRepository) GetRoadmapIDToRoadmapConditions() (map[uint]models.RoadmapConditions, error) {
	conditions := []*models.RoadmapConditions{}

	err := o.db.Table(psql.RoadmapsTable).
		Select(`
			id,
			content_ads_rotation,
			max_profile_email_count`).
		Scan(&conditions).Error
	if err != nil {
		return nil, fmt.Errorf("%v table query error: %v", psql.RoadmapsTable, err)
	}

	result := map[uint]models.RoadmapConditions{}
	for _, condition := range conditions {
		result[condition.RoadmapID] = models.RoadmapConditions{
			RoadmapID:            condition.RoadmapID,
			ContentAdsRotation:   condition.ContentAdsRotation,
			MaxProfileEmailCount: condition.MaxProfileEmailCount,
		}
	}

	return result, nil
}

// GetContentIDToEmailID get from entities content to email ids.
func (o *EntitiesRepository) GetContentIDToEmailID() (map[int]int, error) {
	contentIDToEmailID := []*models.ContentIDEmailID{}

	err := o.db.Table(psql.EmailContentsTables).
		Select(`
			email_id,
			content_id`).
		Joins("inner join site_emails se on email_contents.email_id = se.id").
		Where("email_usage_type = ?", sites.EmailUsageTypeContentSeries).
		Scan(&contentIDToEmailID).Error
	if err != nil {
		return nil, fmt.Errorf("%s table query error: %v", psql.EmailContentsTables, err)
	}

	result := make(map[int]int)
	for _, value := range contentIDToEmailID {
		result[value.ContentID] = value.EmailID
	}

	return result, nil
}
