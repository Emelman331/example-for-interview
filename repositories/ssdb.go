package repositories

import (
	"fmt"
	"strconv"
	"sync"

	"github.com/eryx/lessgo/data/hissdb"
	"github.com/golang/glog"
	"gitlab.regium.com/xr/plan-processor/config"
	"gitlab.regium.com/xr/plan-processor/models"
)

const (
	ssdbQueryGet = "multi_get"
	ssdbQuerySet = "multi_set"
)

// SsdbRepository SSDB repository.
type SsdbRepository struct {
	client    *hissdb.Connector // service cache
	batch     *hissdb.Batch
	batchSize int
	workers   int
}

// NewSsdbRepository init new SsdbRepository instance.
func NewSsdbRepository(cfg *config.SSDBPlanPrcConfig, batchSize, workers int) (*SsdbRepository, error) {
	client, err := hissdb.NewConnector(hissdb.Config{
		Host:    cfg.Host,
		Port:    cfg.Port,
		Timeout: cfg.Timeout,
		MaxConn: cfg.MaxConnections,
	})
	if err != nil {
		return nil, fmt.Errorf("hissdb.NewConnector(%v) error: %v", cfg, err)
	}

	return &SsdbRepository{
		client:    client,
		batch:     client.Batch(),
		batchSize: batchSize,
		workers:   workers,
	}, nil
}

// Close closes connections to database.
func (o *SsdbRepository) Close() {
	o.client.Close()
}

// GetPlansByProfileID get cache by pattern - map[ProdileID]planned contentent letters.
func (o *SsdbRepository) GetPlansByProfileID(keys []string) (map[string]int64, error) {
	res := make(map[string]int64)

	queryWG := &sync.WaitGroup{}
	resCh := make(chan models.KV)

	batchKeysProcess(keys, len(keys)/o.workers, func(batch []string) {
		queryWG.Add(1)

		go o.getLastPlans(queryWG, batch, resCh)
	})

	collectDone := make(chan struct{})

	go func(done chan struct{}) {
		defer func() { done <- struct{}{} }()

		for r := range resCh {
			res[r.Key] = r.Value
		}
	}(collectDone)

	queryWG.Wait()
	close(resCh)

	<-collectDone
	close(collectDone)

	return res, nil
}

func (o *SsdbRepository) getLastPlans(wg *sync.WaitGroup, keys []string, res chan models.KV) {
	defer wg.Done()

	batchKeysProcess(keys, o.batchSize, func(batch []string) {
		reply := o.client.Cmd(ssdbQueryGet, batch)
		if reply.State != hissdb.ReplyOK && reply.State != hissdb.ReplyNotFound {
			glog.Errorf("multi_get query error: %v", reply.State)
		}

		for i := 0; i < len(reply.Data); i += 2 {
			plans, err := strconv.ParseInt(reply.Data[i+1], 10, 64)
			if err != nil {
				glog.Errorf("parse value '%v' error: %v", reply.Data[i+1], err)
				continue
			}

			res <- models.KV{
				Key:   reply.Data[i],
				Value: plans,
			}
		}
	})
}

func batchKeysProcess(
	keys []string,
	batchSize int,
	iteration func(batch []string)) {
	keysLen := len(keys)

	step := batchSize
	if step <= 0 {
		step = 1
	}

	for current := 0; current < keysLen; current += step {
		last := current + step
		if last > keysLen {
			last = keysLen
		}

		iteration(keys[current:last])
	}
}

// ProcessSSDBCacheUpdate update ssdb cache for planed content emails count by profileID.
func (o *SsdbRepository) ProcessSSDBCacheUpdate(profileIDToContentCount map[string]int32) {
	keys := o.convertToKeyValueOrderSlice(profileIDToContentCount)
	queryWG := &sync.WaitGroup{}

	batchKeysProcess(keys, len(keys)/o.workers, func(batch []string) {
		queryWG.Add(1)

		go o.setLastPlans(queryWG, batch)
	})

	queryWG.Wait()
}

func (o *SsdbRepository) convertToKeyValueOrderSlice(keys map[string]int32) []string {
	var result []string

	for key, val := range keys {
		result = append(result, key, string(val))
	}

	return result
}

func (o *SsdbRepository) setLastPlans(wg *sync.WaitGroup, keys []string) {
	defer wg.Done()

	batchKeysProcess(keys, o.batchSize, func(batch []string) {
		reply := o.client.Cmd(ssdbQuerySet, batch)
		if reply.State != hissdb.ReplyOK && reply.State != hissdb.ReplyNotFound {
			glog.Errorf("multi_set query error: %v", reply.State)
		}
	})
}
