package repositories

import (
	"encoding/json"
	"fmt"
	"math/rand"

	"github.com/eryx/lessgo/data/hissdb"
	"github.com/golang/glog"
	"gitlab.regium.com/xr/plan-processor/config"
	"gitlab.regium.com/xr/xutor/model/msgs"
	"gitlab.regium.com/xr/xutor/ssdb"
)

// DecisionRepository SSDB repository.
type DecisionRepository struct {
	decisionConns []*hissdb.Connector
}

// NewDecisionRepository init new SsdbRepository instance.
func NewDecisionRepository(cfg *config.PlanProcessorConfig) (*DecisionRepository, error) {
	deczs, err := initDeczConnectors(cfg)
	if err != nil {
		errMsg := fmt.Errorf("initDeczConnectors(%v, %v) Error: %v", cfg.DecsConfig, cfg.DeczDBs.SDeczInstancesInfo, err)
		return nil, errMsg
	}

	glog.Infoln("connection to decz SSDBs successfully established")

	return &DecisionRepository{
		decisionConns: deczs,
	}, nil
}

// Close closes connections to database.
func (o *DecisionRepository) Close() {
	for _, dc := range o.decisionConns {
		dc.Close()
	}
}

// initDeczConnectors get 'decisions-for-msgen' SSDB connectors.
func initDeczConnectors(cfg *config.PlanProcessorConfig) ([]*hissdb.Connector, error) {
	var conns = []*hissdb.Connector{}

	dbxs, err := cfg.DeczDBs.GetDecisionDBX()
	if err != nil {
		return conns, err
	}

	if len(dbxs) == 0 {
		return conns, fmt.Errorf("decision instances info corrupt")
	}

	// Create connectors, use DecsConfig & decz instances info
	for _, dbx := range dbxs {
		conn, conErr := hissdb.NewConnector(
			hissdb.Config{
				Host:    dbx.Host,
				Port:    dbx.Port,
				Timeout: cfg.DecsConfig.Timeout,
				MaxConn: cfg.DecsConfig.MaxConnections,
			})
		if conErr != nil {
			return conns, conErr
		}

		conns = append(conns, conn)
	}

	return conns, err
}

// getDeczSsdbConnector gets random connector.
func (o *DecisionRepository) getDeczSsdbConnector() (*hissdb.Connector, error) {
	switch len(o.decisionConns) {
	case 0:
		return nil, fmt.Errorf("decz ssdb connectors not found")
	case 1:
		return o.decisionConns[0], nil
	default:
		return o.decisionConns[rand.Intn(len(o.decisionConns))], nil
	}
}

// SaveDecision saves planned email as final decision.
func (o *DecisionRepository) SaveDecision(emfs msgs.EmlFullSpec) error {
	decsConn, err := o.getDeczSsdbConnector()
	if err != nil {
		return err
	}

	if decsConn == nil {
		return fmt.Errorf("decz ssdb connector not found")
	}

	data, err := json.Marshal(emfs)
	if err != nil {
		return err
	}

	if !ssdb.Put(decsConn, emfs.StorageKey, string(data)) {
		return fmt.Errorf("unable to save decision, error: %v, key: %v (connector: %v)", err, emfs.StorageKey, *decsConn)
	}

	return nil
}
