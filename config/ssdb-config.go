package config

import ssdbConfig "gitlab.regium.com/xr/ssdb/config"

// SSDBPlanPrcConfig configuration for ssdb.
type SSDBPlanPrcConfig ssdbConfig.SsdbConfig
