package config

import (
	"gitlab.regium.com/xr/psql/config"
)

// PostgreSQLEntitiesConfig is the config for entities database.
type PostgreSQLEntitiesConfig config.PostgreSQLConfig
