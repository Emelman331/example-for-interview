package config

import (
	"fmt"

	"github.com/golang/glog"
	asc "gitlab.regium.com/xr/aerospike/config"
	xc "gitlab.regium.com/xr/configuration"
	"gitlab.regium.com/xr/rmq"
	sc "gitlab.regium.com/xr/ssdb/config"
)

// PlanProcessorConfig service configuration.
type PlanProcessorConfig struct {
	PostgreSQLEntitiesConfig // Embedded pg entities configuration
	SSDBPlanPrcConfig        // Embedded ssdb configuration
	sc.DeczDBs               // ssdb decisions configuration
	DecsConfig               // Embedded ssdb settings configuration
	asc.AerospikeConfig      // Embedding aerospike config.
	rmq.RMQConfig            // Embedded rabbitMQ configuration

	RMQDeliveryChannelSize int
	RMQPrefetchCount       int
	RMQXrPlanStatsExchange string
	RMQSPExchange          string
	RMQVortexExchange      string
	RMQVortexRoutingKey    string

	ShutdownTimeoutSeconds int

	MessagesBatchSize                int
	TrySaveDurationSeconds           int
	VortexMessagesBatchSize          int
	TrySaveDurationSecForVortexBatch int

	CacheEntitiesUpdateTTLSeconds uint
	CachePlannedLimitsTTLSeconds  uint

	WorkersForAero int

	BatchSizeForSSDB int
	WorkersForSSDB   int

	TTLForEmailFullSpec         int64
	TTLForAerospikeCalendarDays int
}

// NewPlanProcessorConfig return service configuration.
func NewPlanProcessorConfig() (*PlanProcessorConfig, error) {
	planPrcCfg := &PlanProcessorConfig{}

	// Setup Consul connection
	if err := xc.Setup(""); err != nil {
		return planPrcCfg, fmt.Errorf("xc.Setup('') error: %v", err)
	}

	// Fill result from Consul
	if err := xc.Fill(planPrcCfg); err != nil {
		return planPrcCfg, fmt.Errorf("xc.Fill() error: %v", err)
	}

	glog.Infoln(xc.ToString(*planPrcCfg))

	return planPrcCfg, nil
}
