PRJ = xr
APP = plan-processor
BIN = $(APP)
BR = `git name-rev --name-only HEAD`
VER = `git describe --tags --abbrev=0`
COMMIT = `git rev-parse --short HEAD`
TIMESTM = `date -u '+%Y-%m-%d_%H:%M:%S%p'`
FORMAT = v$(VER)-$(COMMIT)-$(TIMESTM)
DOCTAG = $(VER)-$(BR)

.PHONY: integration-test
integration-test:
	sudo docker-compose down --rmi local
	sudo docker-compose build --no-cache
	sudo docker-compose up $(APP)
	sudo docker-compose down

.PHONY: build
build:
	CGO_ENABLED=0 go build -o $(BIN) -ldflags "-X main.BuildVersion=$(FORMAT)"

.PHONY: build-image
build-image:
	sudo docker build -t $(APP):$(DOCTAG) .

.PHONY: push
push:
	sudo docker tag $(APP):$(DOCTAG) docker-xr.regium.com:5000/$(APP):$(DOCTAG)
	sudo docker push docker-xr.regium.com:5000/$(APP):$(DOCTAG)