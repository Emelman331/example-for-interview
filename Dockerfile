FROM scratch

ENV CONSUL_HTTP_ADDR cfg.engagemktg.com:8500

ADD plan-processor ./plan-processor
VOLUME ["/logs"]

CMD ["./plan-processor", "-log_dir", "logs"]