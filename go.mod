module gitlab.regium.com/xr/plan-processor

go 1.16

require (
	github.com/aerospike/aerospike-client-go v4.5.0+incompatible
	github.com/eryx/lessgo v0.0.0-20201010103753-2e2039a4eb3c
	github.com/golang/glog v0.0.0-20210429001901-424d2337a529
	github.com/jinzhu/gorm v1.9.16
	github.com/patrickmn/go-cache v2.1.0+incompatible
	gitlab.regium.com/xr/aerospike v0.0.1
	gitlab.regium.com/xr/configuration v0.0.3
	gitlab.regium.com/xr/psql v0.0.3
	gitlab.regium.com/xr/rmq v0.0.3
	gitlab.regium.com/xr/ssdb v0.0.1
	gitlab.regium.com/xr/xutor v0.0.47
)
