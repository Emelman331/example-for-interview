package main

import (
	"flag"
	"runtime/debug"

	"github.com/golang/glog"
	"gitlab.regium.com/xr/plan-processor/config"
	"gitlab.regium.com/xr/plan-processor/repositories"
	"gitlab.regium.com/xr/plan-processor/services"
	conf "gitlab.regium.com/xr/psql/config"
	"gitlab.regium.com/xr/rmq"
	"gitlab.regium.com/xr/xutor/data/smartplanner"
	"gitlab.regium.com/xr/xutor/errh"
)

var BuildVersion = "" // BuildVersion set on compile time.

func main() {
	defer func() {
		if r := recover(); r != nil {
			glog.Errorf(errh.PANIC_ERR_PTRN, "main", r, string(debug.Stack()))
		}

		glog.Flush()
	}()

	flag.Parse()
	glog.Infof("Build version: %s", BuildVersion)

	cfg, err := config.NewPlanProcessorConfig()
	if err != nil {
		glog.Fatalf("config.NewPlanProcessorConfig() error: %v", err)
	}

	entitiesRepository, err := repositories.NewEntitiesRepository(conf.PostgreSQLConfig(cfg.PostgreSQLEntitiesConfig))
	if err != nil {
		glog.Fatalf("repositories.NewEntitiesRepository(%v) error: %v", cfg.PostgreSQLEntitiesConfig, err)
	}

	defer entitiesRepository.Close()

	entitiesCache := services.NewCache(entitiesRepository, int(cfg.CacheEntitiesUpdateTTLSeconds))

	err = entitiesCache.UpdateOnce()
	if err != nil {
		glog.Fatalf("cache.UpdateOnce() error: %v", err)
	}

	go entitiesCache.Update()

	cacheSsdbRepository, err := repositories.NewSsdbRepository(&cfg.SSDBPlanPrcConfig, cfg.BatchSizeForSSDB, cfg.WorkersForSSDB)
	if err != nil {
		glog.Fatalf("repositories.NewSSDBRepository() error: %v", err)
	}
	defer cacheSsdbRepository.Close()

	// Get 'decisions-for-msgen' SSDB connectors
	deczSsdbRepository, err := repositories.NewDecisionRepository(cfg)
	if err != nil {
		glog.Fatalf("repositories.NewDecisionRepository(%v, %v) Error: %v", cfg.DecsConfig, cfg.DeczDBs.SDeczInstancesInfo, err)
	}
	defer deczSsdbRepository.Close()

	aerospikeRepository, err := repositories.NewAerospikeRepository(cfg.AerospikeConfig, uint(cfg.WorkersForAero))
	if err != nil {
		glog.Fatalf("repositories.NewAerospikeRepository() error: %v", err)
	}
	defer aerospikeRepository.Close()

	consumer, err := services.NewConsumer(
		cfg.RMQConfig,
		entitiesCache.GetPlanProcessorQueue(),
		cfg.RMQDeliveryChannelSize,
		cfg.RMQPrefetchCount)
	if err != nil {
		glog.Fatalf("services.NewConsumer() error: %v", err)
	}

	publisher, err := smartplanner.NewPublisher(cfg.RmqXRSouthParkUri, cfg.RMQSPExchange, cfg.RMQVortexExchange, cfg.RMQVortexRoutingKey)
	if err != nil {
		glog.Fatalf("smartplanner.NewPublisher() error: %v", err)
	}
	defer publisher.Close()

	publisherXr := rmq.NewPublisher(cfg.RmqXRUri)

	err = publisherXr.InitChannel()
	if err != nil {
		glog.Fatalf("rmq.NewPublisher() error: %v", err)
	}

	defer publisherXr.CloseChannel()

	plannedCache, err := services.NewPlansCache(int(cfg.CachePlannedLimitsTTLSeconds))
	if err != nil {
		glog.Fatalf("services.NewPlansCache() error: %v", err)
	}

	defer plannedCache.Save()

	statPublisher := services.NewStatPublisher(
		publisherXr,
		cfg.RMQXrPlanStatsExchange)

	services.NewPlanProcessor(
		consumer,
		publisher,
		statPublisher,
		entitiesCache,
		plannedCache,
		cacheSsdbRepository,
		deczSsdbRepository,
		aerospikeRepository,
		cfg.ShutdownTimeoutSeconds,
		cfg.TTLForEmailFullSpec,
		cfg.TTLForAerospikeCalendarDays,
		cfg.RMQVortexExchange,
		cfg.VortexMessagesBatchSize,
		cfg.TrySaveDurationSecForVortexBatch,
		cfg.RMQSPExchange,
		cfg.MessagesBatchSize,
		cfg.TrySaveDurationSeconds).Start()

	glog.Infoln("plan-processor is shutting down")
}
