package services

import (
	"time"

	"gitlab.regium.com/xr/rmq"
)

const consumerTag = "plan-processor"

// NewConsumer creates prepared RabbitMQ consumer.
func NewConsumer(cfg rmq.RMQConfig, queueName string, deliveryChanSize, prefetchCount int) (*rmq.Consumer, error) {
	consumerConfig := &rmq.ConsumerConfig{
		Name:                  consumerTag,
		ReconnectTimeout:      time.Duration(cfg.RmqReconnectionPeriodSec) * time.Second,
		DeliveryChannBuffSize: deliveryChanSize,
		URI:                   cfg.RmqXRSouthParkUri,
		PrefetchCount:         prefetchCount,
		PrefetchSize:          0,
		PrefetchGlobal:        false,
		Queue:                 queueName,
		AutoAck:               false,
		Exclusive:             false,
		NoLocal:               false,
		NoWait:                false,
	}

	consumer := rmq.NewConsumer(consumerConfig)
	err := consumer.InitConsumer()

	return consumer, err
}
