package services

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"reflect"
	"syscall"
	"time"

	"github.com/aerospike/aerospike-client-go"
	"github.com/golang/glog"
	"gitlab.regium.com/xr/plan-processor/consts"
	"gitlab.regium.com/xr/plan-processor/models"
	"gitlab.regium.com/xr/plan-processor/repositories"
	"gitlab.regium.com/xr/rmq"
	"gitlab.regium.com/xr/xutor/data"
	sp "gitlab.regium.com/xr/xutor/data/smartplanner"
	"gitlab.regium.com/xr/xutor/global"
	"gitlab.regium.com/xr/xutor/model/content"
	"gitlab.regium.com/xr/xutor/model/msgs"
)

type PlanProcessor struct {
	consumer            *rmq.Consumer
	publisher           *sp.Publisher
	cacheSsdbRepository *repositories.SsdbRepository
	deczSsdbRepository  *repositories.DecisionRepository
	aeroRepositoty      *repositories.AerospikeRepository
	entitiesCache       *Cache
	plansCache          *PlansCache
	statPublisher       *StatPublisher
	ssdbPlannedData     map[string]int64

	vortexExchange                   string
	vortexes                         sp.VortexEvents
	vortexChan                       chan *sp.VortexEvent
	vortexBatchSize                  int
	trySaveDurationSecForVortexBatch int

	planSpExchange             string
	planSpBatchSize            int
	planSpbatchTrySaveDuration int
	queueToCollector           map[string]*sp.Collector

	shutdownTimeoutInSec int
	osSignalsChan        chan os.Signal

	planTargetTime        time.Time
	ttlForEmailFullSpec   int64
	ttlForCalendarSeconds uint32
}

func NewPlanProcessor(
	consumer *rmq.Consumer,
	publisher *sp.Publisher,
	statPublisher *StatPublisher,
	entitiesCache *Cache,
	plansCache *PlansCache,
	cacheSsdbRepository *repositories.SsdbRepository,
	deczSsdbRepository *repositories.DecisionRepository,
	aeroRepositoty *repositories.AerospikeRepository,
	shutdownTimeoutInSec int,
	ttlForEmailFullSpec int64,
	ttlForCalendarDays int,
	vortexExchangeName string,
	vortexBatchSize,
	trySaveDurationSecForVortexBatch int,
	planSpExchange string,
	planSpBatchSize int,
	planSpbatchTrySaveDuration int) *PlanProcessor {
	osSignalsChan := make(chan os.Signal, 1)
	signal.Notify(osSignalsChan, os.Interrupt, syscall.SIGTERM, syscall.SIGUSR1)

	return &PlanProcessor{
		consumer:            consumer,
		publisher:           publisher,
		statPublisher:       statPublisher,
		entitiesCache:       entitiesCache,
		plansCache:          plansCache,
		ssdbPlannedData:     make(map[string]int64),
		cacheSsdbRepository: cacheSsdbRepository,
		deczSsdbRepository:  deczSsdbRepository,
		aeroRepositoty:      aeroRepositoty,

		vortexExchange:                   vortexExchangeName,
		vortexes:                         sp.VortexEvents{},
		vortexChan:                       make(chan *sp.VortexEvent),
		vortexBatchSize:                  vortexBatchSize,
		trySaveDurationSecForVortexBatch: trySaveDurationSecForVortexBatch,

		planSpExchange:             planSpExchange,
		planSpBatchSize:            planSpBatchSize,
		planSpbatchTrySaveDuration: planSpbatchTrySaveDuration,
		queueToCollector:           make(map[string]*sp.Collector),

		osSignalsChan:        osSignalsChan,
		shutdownTimeoutInSec: shutdownTimeoutInSec,

		ttlForEmailFullSpec:   ttlForEmailFullSpec,
		ttlForCalendarSeconds: uint32(ttlForCalendarDays * global.SecondsInDay),
	}
}

func (o *PlanProcessor) Start() {
	defer func() {
		o.consumer.StopConsume()

		close(o.consumer.DeliveryChannel)
		close(o.osSignalsChan)
		close(o.vortexChan)
		o.statPublisher.Close()

		time.Sleep(time.Duration(o.shutdownTimeoutInSec) * time.Second)
	}()

	go o.processVortexes()
	go o.statPublisher.ProcessPlanned()

	o.processRMQEvents()
}

func (o *PlanProcessor) processRMQEvents() {
	o.consumer.StartConsume()

	for {
		select {
		case osSignal := <-o.osSignalsChan:
			glog.Infof("plan-processor got stop signal %v -> start shutting down...", osSignal)
			glog.Flush()

			return
		case delivery, ok := <-o.consumer.DeliveryChannel:
			if !ok {
				glog.Errorf("plan-processor can't get delivery - channel is not ok")

				return
			}

			var messages []*sp.Message

			err := json.Unmarshal(delivery.Body, &messages)
			if err != nil {
				glog.Errorf("deserialize rmq event error: %v", err)
				o.consumer.Ack(delivery, false)

				continue
			}

			ssdbKeys := o.processRmqMessagesToSSDBKeys(messages)

			// get count of planned content emails in a row by profile.
			o.ssdbPlannedData, err = o.cacheSsdbRepository.GetPlansByProfileID(ssdbKeys)
			if err != nil {
				glog.Errorf("cant read plan data from ssdb. error: %v", err)

				return
			}

			if len(messages) > 0 {
				o.processMessages(messages)
			}

			o.consumer.Ack(delivery, false)
		}
	}
}

func (o *PlanProcessor) processMessages(messages []*sp.Message) {
	profileToContentCount := make(map[string]int32)
	currentTime := time.Now().UTC()

	for _, message := range messages {
		filter := o.entitiesCache.GetFilterByRoadmapID(message.RoadmapID)

		profileKey := models.ProfileContentPlans{ProfileID: message.ProfileID}
		currentProfilePlansByHour := o.plansCache.Get(profileKey)

		// checking if messages are out of date
		if message.Runstamp+global.SecondsInHour < currentTime.Unix() {
			o.vortexChan <- getVortexErrorEvent(message, sp.PPErrorMessageRunstampOutdated)
			continue
		}

		if currentProfilePlansByHour >= filter.MaxProfileEmailCount {
			o.vortexChan <- getVortexErrorEvent(message, sp.PPErrorPlanForHourLimitExceeded)
			continue
		}

		profileKey.ContentCounter = int32(o.ssdbPlannedData[profileKey.String()])

		if message.SerieTypeID == global.SysContentSerieContentId {
			profileKey.ContentCounter++
		}

		if message.SerieTypeID == global.SysContentSerieAdsId {
			if profileKey.ContentCounter < filter.ContentAdsRotation {
				o.vortexChan <- getVortexErrorEvent(message, sp.PPErrorContentAdsRotation)
				continue
			}

			profileKey.ContentCounter = 0
		}

		if o.processPlannedMessage(message) {
			profileToContentCount[profileKey.String()] = profileKey.ContentCounter
			o.plansCache.Set(profileKey, currentProfilePlansByHour+1)
		}
	}

	o.cacheSsdbRepository.ProcessSSDBCacheUpdate(profileToContentCount)
}

func (o *PlanProcessor) processRmqMessagesToSSDBKeys(messages []*sp.Message) (keys []string) {
	for _, message := range messages {
		plannedContentsByProfileID := models.ProfileContentPlans{ProfileID: message.ProfileID}
		keys = append(keys, plannedContentsByProfileID.String())
	}

	return keys
}

func (o *PlanProcessor) processPlannedMessage(message *sp.Message) bool {
	rec, err := o.aeroRepositoty.GetProfile(message.ProfileID)
	if err != nil {
		glog.Errorf("get profile error: %v", err)
		return false
	}

	fullSpec, err := o.GetEmailFullSpec(message, rec)
	if err != nil {
		glog.Errorf("cant get email fullspec from message: %v, error: %v", message, err)
		return false
	}

	calendar, err := o.aeroRepositoty.GetCalendar(message.ProfileID, o.planTargetTime.Unix())
	if err != nil {
		glog.Errorln(err)
		return false
	}

	spec, err := o.writeCalendar(message, fullSpec, calendar)
	if err != nil || spec == nil {
		glog.Errorf("write calendar issue, error: %v, spec: %v", err, spec)
		return false
	}

	// Save to final decision ssdb
	err = o.deczSsdbRepository.SaveDecision(spec.EmlFullSpec)
	if err != nil {
		glog.Errorf("save final spec err: %v", err)
		return false
	}

	o.statPublisher.SendMessageToPlan(getStatPlanEvent(spec, int(message.SiteID)))
	
	err = o.SendMessageToSpStatistics(message)
	if err != nil {
		o.vortexChan <- getVortexErrorEvent(message, sp.PPErrorCantSendMessage)
	}

	return true
}

// GetEmailFullSpec get email full spec object.
func (o *PlanProcessor) GetEmailFullSpec(message *sp.Message, rec *aerospike.Record) (*msgs.EmlFullSpec, error) {
	bin, ok := rec.Bins[global.EmailKeyNamePtrn]
	if !ok || bin == nil {
		errMsg := fmt.Errorf("cant get bin from record. binName: %v, bin data: %v", global.EmailKeyNamePtrn, rec.Bins)
		return nil, errMsg
	}

	emailName, ok := bin.(string)
	if !ok {
		errMsg := fmt.Errorf("undefined type for email name, want: string, got: %v", reflect.TypeOf(bin))
		return nil, errMsg
	}

	if len(message.ContentIDs) == 0 {
		errMsg := fmt.Errorf("contentIDs is empty, message: %v", message)
		return nil, errMsg
	}

	if len(message.SerieIDs) == 0 {
		errMsg := fmt.Errorf("seriesIDs is empty, message: %v", message)
		return nil, errMsg
	}

	externalContentID, err := content.GetExternalContentIDFromContentIDM(int64(message.ContentIDs[0]))
	if err != nil {
		errMsg := fmt.Errorf("content.GetExternalContentIDFromContentIDM(%v) err: %v, message: %v", message.ContentIDs[0], err, message)
		return nil, errMsg
	}

	xrContent := content.GetXRContentIDFromContentIDM(int64(message.ContentIDs[0]))
	emailID := o.entitiesCache.GetEmailIDByContentID(xrContent)

	return &msgs.EmlFullSpec{
		EmlSpec: msgs.EmlSpec{
			EmailId:      emailID,
			HighPriority: false,
			RunTime:      message.Runstamp,
			Pool_ID:      consts.DefaultPoolID,
		},

		PK:                  message.ProfileID,
		EmailName:           emailName,
		ContentIds:          []int{xrContent},
		ExternalContentId:   externalContentID,
		ContentSeriesId:     int(message.SerieIDs[0]),
		ContentSeriesTypeId: int(message.SerieTypeID),
		StorageKey:          fmt.Sprint(message.Runstamp, consts.SpecStorageKeySeparator, message.ProfileID, emailID),
		Test:                false,

		TTL: o.ttlForEmailFullSpec,
	}, nil
}

func (o *PlanProcessor) writeCalendar(
	message *sp.Message,
	fullSpec *msgs.EmlFullSpec,
	calendar *models.Calendar) (*msgs.FullSpecInfo, error) {
	var slotTimeStamp, currentSlotNumber int64

	for ts := range calendar.Slots {
		slotOffSet := consts.SecondsInFifteenMinutes * currentSlotNumber
		slotBaseTimestamp := o.planTargetTime.Unix() + slotOffSet + int64(message.TimeZone)
		currentSlotNumber++

		if ts >= slotBaseTimestamp && ts < slotBaseTimestamp+consts.SecondsInFifteenMinutes {
			continue
		}

		bias := rand.Int63n(consts.SecondsInFifteenMinutes)
		slotTimeStamp = slotBaseTimestamp + bias

		break
	}

	if currentSlotNumber >= consts.MaxCalendarSlotNumber {
		o.vortexChan <- getVortexErrorEvent(message, sp.PPErrorNoFreeSlotsInCalendar)
		return nil, fmt.Errorf("calendar slot limit for hour is reached")
	}

	calendar.Slots[slotTimeStamp] = fullSpec.EmailId

	result := &msgs.FullSpecInfo{}
	result.PlanTime = slotTimeStamp
	result.EmlFullSpec = *fullSpec

	err := o.aeroRepositoty.Upsert(calendar, o.ttlForCalendarSeconds)
	if err != nil {
		return nil, fmt.Errorf("o.aeroRepositoty.Upsert(%v, %v), error: %v", calendar, o.ttlForCalendarSeconds, err)
	}

	return result, nil
}

func (o *PlanProcessor) processVortexes() {
	defer o.publishVortexes()

	ticker := time.NewTicker(time.Duration(o.trySaveDurationSecForVortexBatch) * time.Second)

	for {
		select {
		case <-ticker.C:
			o.publishVortexes()

		case vortex, ok := <-o.vortexChan:
			if !ok {
				return
			}

			o.vortexes = append(o.vortexes, *vortex)

			if len(o.vortexes) >= o.vortexBatchSize {
				o.publishVortexes()
			}
		}
	}
}

func (o *PlanProcessor) publishVortexes() {
	if len(o.vortexes) == 0 {
		return
	}

	err := o.publisher.PublishVortexMessage(o.vortexes)
	if err != nil {
		glog.Errorf("publish vortexes error: %v", err)
	}

	o.vortexes = sp.VortexEvents{}
}

func getVortexErrorEvent(message *sp.Message, code uint) *sp.VortexEvent {
	var contentID int64

	if len(message.ContentIDs) > 0 {
		contentID = int64(message.ContentIDs[0])
	}

	return &sp.VortexEvent{
		Runstamp:     message.Runstamp,
		PID:          message.ProfileID,
		SiteID:       message.SiteID,
		SeriesTypeID: message.SerieTypeID,
		SerieID:      message.SerieIDs[0],
		ContentID:    contentID,
		ClassID:      message.ClassID,
		RoadmapID:    message.RoadmapID,
		ServiceID:    sp.PlanProcessorID,
		ErrCode:      code,
	}
}

func getStatPlanEvent(spec *msgs.FullSpecInfo, siteID int) *data.PlanEvent {
	return &data.PlanEvent{
		Timestamp:      spec.PlanTime,
		EmailID:        spec.EmailId,
		ProfileID:      spec.PK,
		Runstamp:       spec.RunTime,
		SiteID:         siteID,
		ContentID:      spec.ContentIds[0],
		ContentSerieID: spec.ContentSeriesId,
	}
}

// SendMessageToSpStatistics send message statistics.
func (o *PlanProcessor) SendMessageToSpStatistics(message *sp.Message) error {
	collector, isExist := o.queueToCollector[consts.SpStatQueue]
	if !isExist {
		collector = sp.NewCollector(consts.SpStatQueue, o.planSpbatchTrySaveDuration, o.planSpBatchSize, o.publisher)
		go collector.Start()

		o.queueToCollector[consts.SpStatQueue] = collector
	}

	collector.SendMessage(message)

	return nil
}
