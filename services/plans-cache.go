package services

import (
	"fmt"
	"os"
	"time"

	"github.com/golang/glog"
	"github.com/patrickmn/go-cache"
)

const (
	cacheFileName   = "/opt/gocache.file"
	cleanupInterval = 10 * time.Minute
)

// PlansCache contains counter of content email planned in a row by profile.
type PlansCache struct {
	c *cache.Cache
}

// NewPlansCache creates new PlansCache instance.
func NewPlansCache(ttlSec int) (*PlansCache, error) {
	c := cache.New(time.Duration(ttlSec)*time.Second, cleanupInterval)

	if _, err := os.Stat(cacheFileName); !os.IsNotExist(err) {
		err := c.LoadFile(cacheFileName)
		if err != nil {
			return nil, err
		}
	}

	return &PlansCache{c: c}, nil
}

// Set upserts new value to cache.
func (o *PlansCache) Set(key fmt.Stringer, value int32) {
	o.c.Set(key.String(), value, 0) // With default TTL
}

// Get gets planned count from cache.
func (o *PlansCache) Get(key fmt.Stringer) int32 {
	value, _ := o.c.Get(key.String())
	res, _ := value.(int32)

	return res
}

// Save saves cache to file.
func (o *PlansCache) Save() {
	err := o.c.SaveFile(cacheFileName)
	if err != nil {
		glog.Errorf("o.c.SaveFile(%v) error: %v", cacheFileName, err)
	}
}
