package services

import (
	"github.com/golang/glog"
	"gitlab.regium.com/xr/rmq"
	"gitlab.regium.com/xr/xutor/data"
)

type StatPublisher struct {
	publisherXr    *rmq.Publisher
	planXrExchange string
	planChan       chan *data.PlanEvent
}

func NewStatPublisher(
	publisherXr *rmq.Publisher,
	planXrExchange string) *StatPublisher {
	return &StatPublisher{
		publisherXr:    publisherXr,
		planXrExchange: planXrExchange,
		planChan:       make(chan *data.PlanEvent),
	}
}

func (o *StatPublisher) SendMessageToPlan(message *data.PlanEvent) {
	o.planChan <- message
}

// ProcessPlanned process rmq events publish.
func (o *StatPublisher) ProcessPlanned() {
	for {
		planMessage, ok := <-o.planChan
		if !ok {
			return
		}

		// Produce planned statistic
		err := o.publisherXr.PublishMessage(o.planXrExchange, o.planXrExchange, planMessage)
		if err != nil {
			glog.Errorf("cant publish message: %v, exchage:(%s) error: %v", planMessage, o.planXrExchange, err)
		}
	}
}

func (o *StatPublisher) Close() {
	close(o.planChan)
}
