package services

import (
	"fmt"
	"sync"
	"time"

	"github.com/golang/glog"
	"gitlab.regium.com/xr/plan-processor/models"
	"gitlab.regium.com/xr/plan-processor/repositories"
)

// Cache contains service cached data.
type Cache struct {
	entitiesRepository    *repositories.EntitiesRepository
	updateIntervalSeconds int
	planProcessorQueue    string

	roadmapIDToConditions map[uint]models.RoadmapConditions
	contentIDToEmailID    map[int]int

	mu *sync.RWMutex
}

// NewCache creates new Cache instance.
func NewCache(entitiesRepository *repositories.EntitiesRepository,
	updateIntervalSeconds int) *Cache {
	return &Cache{
		entitiesRepository:    entitiesRepository,
		updateIntervalSeconds: updateIntervalSeconds,
		roadmapIDToConditions: make(map[uint]models.RoadmapConditions),
		contentIDToEmailID:    make(map[int]int),

		mu: &sync.RWMutex{},
	}
}

// UpdateOnce refreshes cache once.
func (o *Cache) UpdateOnce() error {
	processor, err := o.entitiesRepository.GetPlanProcessor()
	if err != nil {
		return fmt.Errorf("o.entitiesRepository.GetPlanProcessor() error: %v", err)
	}

	o.mu.Lock()
	o.planProcessorQueue = processor.Queue
	o.mu.Unlock()

	return o.update()
}

// Update refreshes cache periodically.
func (o *Cache) Update() {
	interval := time.Duration(o.updateIntervalSeconds) * time.Second
	for range time.Tick(interval) {
		err := o.update()
		if err != nil {
			glog.Errorf("o.update() error: %v", err)
		}
	}
}

func (o *Cache) update() error {
	roadmapIDToConditions, err := o.entitiesRepository.GetRoadmapIDToRoadmapConditions()
	if err != nil {
		return fmt.Errorf("o.entitiesRepository.GetRoadmapIDToRoadmapConditions() error: %v", err)
	}

	contentIDToEmailID, err := o.entitiesRepository.GetContentIDToEmailID()
	if err != nil {
		return fmt.Errorf("o.entitiesRepository.GetContentIDToEmailID() error: %v", err)
	}

	o.mu.Lock()
	o.roadmapIDToConditions = roadmapIDToConditions
	o.contentIDToEmailID = contentIDToEmailID
	o.mu.Unlock()

	glog.Infoln(o)

	return nil
}

// GetPlanProcessorQueue gets current processor queue.
func (o *Cache) GetPlanProcessorQueue() string {
	o.mu.RLock()
	defer o.mu.RUnlock()

	return o.planProcessorQueue
}

func (o *Cache) GetFilterByRoadmapID(roadmapID uint) models.RoadmapConditions {
	o.mu.RLock()
	defer o.mu.RUnlock()

	return o.roadmapIDToConditions[roadmapID]
}

func (o *Cache) GetEmailIDByContentID(contentID int) int {
	o.mu.RLock()
	defer o.mu.RUnlock()

	return o.contentIDToEmailID[contentID]
}

// String returns human readable cache content.
func (o Cache) String() string {
	return fmt.Sprintf(`cache:
		planProcessorQueue: %v,
		roadmapIDToPlanConditions: %v,
		contentIDToEmailID: %v`,
		o.planProcessorQueue,
		o.roadmapIDToConditions,
		o.contentIDToEmailID,
	)
}
