package models

// RoadmapConditions contains plan conditions for ads and content series. And max planned for hour by profile.
type RoadmapConditions struct {
	RoadmapID            uint  `gorm:"column:id"`
	ContentAdsRotation   int32 `gorm:"column:content_ads_rotation"`
	MaxProfileEmailCount int32 `gorm:"column:max_profile_email_count"`
}
