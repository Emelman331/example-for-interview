package models

// ContentIDEmailID struct for entities cache.
type ContentIDEmailID struct {
	EmailID   int `gorm:"column:email_id"`
	ContentID int `gorm:"column:content_id"`
}
