package models

// Processor contains smart planner processor model.
type Processor struct {
	ID    uint16 `gorm:"column:id"`
	Queue string `gorm:"column:queue"`
}
