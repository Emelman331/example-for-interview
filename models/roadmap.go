package models

// Roadmap contains roadmap model.
type Roadmap struct {
	ID    uint16 `gorm:"column:roadmap_id"`
	Queue string `gorm:"column:queue"`
}
