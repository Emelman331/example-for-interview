package models

import "fmt"

// ProfileContentPlans struct represents how many contents planned for profile in a row.
type ProfileContentPlans struct {
	ProfileID      int64
	ContentCounter int32
}

// String method provides string representation of the ProfileID key.
func (o ProfileContentPlans) String() string {
	return fmt.Sprint(o.ProfileID)
}
