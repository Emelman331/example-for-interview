package models

// KV represents key-value pair.
type KV struct {
	Key   string
	Value int64
}
