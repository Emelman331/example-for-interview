package models

// Calendar model struct.
type Calendar struct {
	Pid      int64         `as:pid`       // Profile ID.
	DayStart int64         `as:day_start` // Start of a day by profile's timezone in UTC.
	Slots    map[int64]int `as:slots`     // 15-minutes send slot for a day of a calendar.
}
