package consts

import "gitlab.regium.com/xr/xutor/global"

const (
	SpecStorageKeySeparator = "|"
	DefaultPoolID           = 0
	SecondsInFifteenMinutes = global.SecondsInMinute * 15
	MaxCalendarSlotNumber   = 4
	SpStatQueue             = "stat_aggregator"
)
